#ifndef _PALETTE_H_
#define _PALETTE_H_

class Palette
{
public:
    Palette();
    ~Palette();
    int IndexToColour(char index);
};

#endif