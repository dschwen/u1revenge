#ifndef _BOTTOMRIGHTVIEW_H_
#define _BOTTOMRIGHTVIEW_H_

#include <SDL.h>
#include <SDL_ttf.h>

#include "View.h"
#include "Player.h"

class BottomRightView : public View
{
private:
    TTF_Font* font;
    Player* player;
    int scale;
    SDL_Color fontColour;
    SDL_Color redFontColour;
    int valueOffset; // x-offset between position of a key (e.g. "Hits:" and its value)

    SDL_Surface* hitsKeyFontSurface;
    SDL_Texture* hitsKeyFontTexture;
    SDL_Rect hitsRect;

    SDL_Surface* foodKeyFontSurface;
    SDL_Texture* foodKeyFontTexture;
    SDL_Rect foodRect;

    SDL_Surface* expKeyFontSurface;
    SDL_Texture* expKeyFontTexture;
    SDL_Rect expRect;

    SDL_Surface* coinKeyFontSurface;
    SDL_Texture* coinKeyFontTexture;
    SDL_Rect coinRect;

    void SetupStatic(SDL_Surface*& surface, SDL_Texture*& texture, const char* str, SDL_Rect& rect);
    void RenderDynamic(unsigned short value, SDL_Color colour, int x, int y);
public:
    BottomRightView(SDL_Renderer* renderer, TTF_Font* font, Player* player, int scale);
    ~BottomRightView();
    void Render();
    void Cleanup();
};

#endif