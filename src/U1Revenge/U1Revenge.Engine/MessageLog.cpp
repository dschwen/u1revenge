#include <string>
#include <sstream>

#include "MessageLog.h"

using std::string;
using std::stringstream;
using std::getline;

// private methods

void MessageLog::PushPrivate(string message)
{
    // message log can have up to 3 lines of text; 4th line is blank waiting for input

    for (int i = 0; i < 2; i++)
        this->lines[i] = this->lines[i + 1];

    this->lines[2] = message;
}

// public methods

MessageLog::MessageLog()
    : lines(4)
{
    for (int i = 0; i < 4; i++)
        this->lines[i] = "";
}

MessageLog::~MessageLog()
{
}

void MessageLog::Push(char* message)
{
    // split into lines

    stringstream ss(message);

    string line;

    while (getline(ss, line, '\n'))
        this->PushPrivate(line);
}

void MessageLog::GetMessages(std::string& s) const
{
    for (int i = 0; i < 4; i++)
    {
        if (i > 0)
            s += "\n";
        s += this->lines[i];
    }
}
