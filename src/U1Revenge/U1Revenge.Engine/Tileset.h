#ifndef _TILESET_H_
#define _TILESET_H_

#include "Palette.h"

class Tileset
{
private:
    Palette* palette;
    int* pixels;
    int tileWidth;
    int tileHeight;
    int tileCount;
public:
    Tileset(Palette* palette);
    ~Tileset();
    void Load(const char* filePath);
    int* GetPixels(char tileIndex) const;
};

#endif