#include "Player.h"
#include "MessageLog.h"

// private methods

void Player::IncrementMoveCount()
{
    this->moveCount++;

    // deduct food every 2 moves

    if (this->moveCount % 2 == 0)
        this->food--;
}


// public methods

Player::Player(int x, int y, MessageLog* messageLog)
    : x(x), y(y), messageLog(messageLog), transport(Foot),
        hits(150), food(200), experience(0), coin(100)
{
}

Player::~Player()
{
}

PlayerTransport Player::GetTransport() const
{
    return this->transport;
}

int Player::GetX() const
{
    return this->x;
}

int Player::GetY() const
{
    return this->y;
}

void Player::MoveLeft()
{
    this->x--;
    if (this->x < 0)
        this->x = 167;

    this->IncrementMoveCount();
    this->messageLog->Push("West");
}

void Player::MoveRight()
{
    this->x++;
    if (this->x > 167)
        this->x = 0;

    this->IncrementMoveCount();
    this->messageLog->Push("East");
}

void Player::MoveUp()
{
    this->y--;
    if (this->y < 0)
        this->y = 155;

    this->IncrementMoveCount();
    this->messageLog->Push("North");
}

void Player::MoveDown()
{
    this->y++;
    if (this->y > 155)
        this->y = 0;

    this->IncrementMoveCount();
    this->messageLog->Push("South");
}

void Player::Pass()
{
    this->IncrementMoveCount();
    this->messageLog->Push("Pass");
}

void Player::CycleTransportLeft()
{
    switch (transport)
    {
    case Foot:
        transport = TimeMachine;
        break;
    case Horse:
        transport = Foot;
        break;
    case Cart:
        transport = Horse;
        break;
    case Raft:
        transport = Cart;
        break;
    case Frigate:
        transport = Raft;
        break;
    case Aircar:
        transport = Frigate;
        break;
    case Shuttle:
        transport = Aircar;
        break;
    case TimeMachine:
        transport = Shuttle;
        break;
    }
}

void Player::CycleTransportRight()
{
    switch (transport)
    {
        case Foot:
            transport = Horse;
            break;
        case Horse:
            transport = Cart;
            break;
        case Cart:
            transport = Raft;
            break;
        case Raft:
            transport = Frigate;
            break;
        case Frigate:
            transport = Aircar;
            break;
        case Aircar:
            transport = Shuttle;
            break;
        case Shuttle:
            transport = TimeMachine;
            break;
        case TimeMachine:
            transport = Foot;
            break;
    }
}

unsigned short Player::GetHits() const
{
    return this->hits;
}

unsigned short Player::GetFood() const
{
    return this->food;
}

unsigned short Player::GetExperience() const
{
    return this->experience;
}

unsigned short Player::GetCoin() const
{
    return this->coin;
}
