#ifndef _SOSARIAINPUTCONTROLLER_H_
#define _SOSARIAINPUTCONTROLLER_H_

#include <SDL.h>

#include "GameEngine.h"
#include "Player.h"
#include "World.h"
#include "MessageLog.h"

class SosariaInputController
{
private:
    GameEngine* gameEngine;
    Player* player;
    World* world;
    MessageLog* messageLog;
public:
    SosariaInputController(GameEngine* gameEngine, Player* player,
        World* world, MessageLog* messageLog);
    ~SosariaInputController();
    bool ProcessInputEvent(SDL_Event& event);
};

#endif