#ifndef _MESSAGELOG_H_
#define _MESSAGELOG_H_

#include <vector>
#include <string>

using std::vector;
using std::string;

class MessageLog
{
private:
    vector<string> lines;
    void PushPrivate(string message);
public:
    MessageLog();
    ~MessageLog();
    void Push(char* message);
    void GetMessages(string& s) const;
};

#endif