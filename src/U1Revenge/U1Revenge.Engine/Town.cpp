#include "Town.h"

Town::Town(const char* name, int x, int y, int type)
    : Location(name, "the city of ", x, y), type(type)
{
}

Town::~Town()
{
}

int Town::GetType()
{
    return this->type;
}
