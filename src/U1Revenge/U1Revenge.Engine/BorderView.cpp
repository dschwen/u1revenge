#include "BorderView.h"

BorderView::BorderView(SDL_Renderer* renderer)
    : View(renderer)
{
}

BorderView::~BorderView()
{
}

void BorderView::Render()
{
    auto renderer = this->GetRenderer();

    // render borders - note these don't scale at the moment

    SDL_SetRenderDrawColor(renderer, 0, 0, 176, 255); // blue

    SDL_RenderFillRect(renderer, &this->topBorder);
    SDL_RenderFillRect(renderer, &this->leftBorder);
    SDL_RenderFillRect(renderer, &this->rightBorder);
    SDL_RenderFillRect(renderer, &this->bottomBorder);
    SDL_RenderFillRect(renderer, &this->bottomSideBorder);

    // render white lines over blue borders

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // white

    SDL_RenderFillRect(renderer, &this->viewportTopHorizontalWhite);
    SDL_RenderFillRect(renderer, &this->viewportLeftVerticalWhite);
    SDL_RenderFillRect(renderer, &this->viewportRightVerticalWhite);
    SDL_RenderFillRect(renderer, &this->viewportBottomHorizontalWhite);

    SDL_RenderFillRect(renderer, &this->messageLogTopWhite);
    SDL_RenderFillRect(renderer, &this->messageLogRightWhite);
    SDL_RenderFillRect(renderer, &this->playerStatsLeftWhite);
    SDL_RenderFillRect(renderer, &this->playerStatsTopWhite);
}
