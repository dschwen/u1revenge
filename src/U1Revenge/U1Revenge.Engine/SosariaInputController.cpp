#include "SosariaInputController.h"

SosariaInputController::SosariaInputController(GameEngine* gameEngine, Player* player,
    World* world, MessageLog* messageLog)
    : gameEngine(gameEngine), player(player), world(world), messageLog(messageLog)
{
}

SosariaInputController::~SosariaInputController()
{
}

bool SosariaInputController::ProcessInputEvent(SDL_Event& event)
{
    switch (event.type)
    {
        case SDL_QUIT:
            return false;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
                case SDLK_LEFT:  player->MoveLeft(); break;
                case SDLK_RIGHT: player->MoveRight(); break;
                case SDLK_UP:    player->MoveUp(); break;
                case SDLK_DOWN:  player->MoveDown(); break;
                case SDLK_SPACE: player->Pass(); break;
                case SDLK_F10:   gameEngine->ToggleFullScreen(); break;
                case SDLK_LEFTBRACKET:  player->CycleTransportLeft(); break;
                case SDLK_RIGHTBRACKET: player->CycleTransportRight(); break;
                case SDLK_i:
                {
                    messageLog->Push("Inform and search");
                    const char* name = world->GetLocationName(player->GetX(), player->GetY());
                    messageLog->Push((char *)name);
                    break;
                }
            }
            break;
    }

    return true;
}
