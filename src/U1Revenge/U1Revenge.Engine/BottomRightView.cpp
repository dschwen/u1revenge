#include <string>

#include "BottomRightView.h"

// private methods

void BottomRightView::SetupStatic(SDL_Surface*& surface, SDL_Texture*& texture, const char* str, SDL_Rect& rect)
{
    auto renderer = this->GetRenderer();

    // TODO recreating these every time seems very inefficient
    surface = TTF_RenderText_Blended_Wrapped(font, str, this->fontColour, 300);
    texture = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);

    SDL_RenderCopy(renderer, texture, NULL, &rect);
}

void BottomRightView::RenderDynamic(unsigned short value, SDL_Color colour, int x, int y)
{
    auto renderer = this->GetRenderer();

    std::string valueStr = std::to_string(value);

    // right-align
    if (value < 10)
        valueStr = " " + valueStr;
    if (value < 100)
        valueStr = " " + valueStr;
    if (value < 1000)
        valueStr = " " + valueStr;

    SDL_Surface* surface = TTF_RenderText_Blended_Wrapped(this->font, valueStr.c_str(), colour, 300);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_Rect dstrect = { x, y, 0, 0 };
    SDL_QueryTexture(texture, NULL, NULL, &dstrect.w, &dstrect.h);

    SDL_RenderCopy(renderer, texture, NULL, &dstrect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}

// public methods

BottomRightView::BottomRightView(SDL_Renderer* renderer, TTF_Font* font, Player* player, int scale)
    : View(renderer), font(font), player(player), scale(scale), fontColour({ 84, 255, 255 }),
        redFontColour({ 255, 80, 81 }), valueOffset(80),
        hitsKeyFontSurface(nullptr), hitsKeyFontTexture(nullptr), hitsRect({ 248 * scale, 168 * scale, 0, 0 }),
        foodKeyFontSurface(nullptr), foodKeyFontTexture(nullptr), foodRect({ 248 * scale, 176 * scale, 0, 0 }),
        expKeyFontSurface(nullptr), expKeyFontTexture(nullptr), expRect({ 248 * scale, 184 * scale, 0, 0 }),
        coinKeyFontSurface(nullptr), coinKeyFontTexture(nullptr), coinRect({ 248 * scale, 192 * scale, 0, 0 })
{
    this->SetupStatic(this->hitsKeyFontSurface, this->hitsKeyFontTexture, "Hits:", this->hitsRect);
    this->SetupStatic(this->foodKeyFontSurface, this->foodKeyFontTexture, "Food:", this->foodRect);
    this->SetupStatic(this->expKeyFontSurface, this->expKeyFontTexture, "Exp.:", this->expRect);
    this->SetupStatic(this->coinKeyFontSurface, this->coinKeyFontTexture, "Coin:", this->coinRect);
}

BottomRightView::~BottomRightView()
{
    this->Cleanup();
}

void BottomRightView::Render()
{
    auto renderer = this->GetRenderer();

    // render static fields (Hits, Food, Exp. and Coin)

    SDL_RenderCopy(renderer, this->hitsKeyFontTexture, NULL, &this->hitsRect);
    SDL_RenderCopy(renderer, this->foodKeyFontTexture, NULL, &this->foodRect);
    SDL_RenderCopy(renderer, this->expKeyFontTexture, NULL, &this->expRect);
    SDL_RenderCopy(renderer, this->coinKeyFontTexture, NULL, &this->coinRect);

    // render dynamic values

    unsigned short hits = this->player->GetHits();
    unsigned short food = this->player->GetFood();
    unsigned short experience = this->player->GetExperience();
    unsigned short coin = this->player->GetCoin();
    
    this->RenderDynamic(hits, hits < 100 ? this->redFontColour : this->fontColour, this->hitsRect.x + this->valueOffset, this->hitsRect.y);
    this->RenderDynamic(food, food < 100 ? this->redFontColour : this->fontColour, this->foodRect.x + this->valueOffset, this->foodRect.y);
    this->RenderDynamic(experience, this->fontColour, this->expRect.x + this->valueOffset, this->expRect.y);
    this->RenderDynamic(coin, this->fontColour, this->coinRect.x + this->valueOffset, this->coinRect.y);
}

void BottomRightView::Cleanup()
{
    SDL_DestroyTexture(this->hitsKeyFontTexture);
    SDL_FreeSurface(this->hitsKeyFontSurface);
    SDL_DestroyTexture(this->foodKeyFontTexture);
    SDL_FreeSurface(this->foodKeyFontSurface);
    SDL_DestroyTexture(this->expKeyFontTexture);
    SDL_FreeSurface(this->expKeyFontSurface);
    SDL_DestroyTexture(this->coinKeyFontTexture);
    SDL_FreeSurface(this->coinKeyFontSurface);
}
