#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "PlayerTransport.h"
#include "MessageLog.h"

class Player
{
private:
    int x; // player location
    int y; // player location
    int moveCount;
    PlayerTransport transport;
    unsigned short hits;
    unsigned short food;
    unsigned short experience;
    unsigned short coin;
    MessageLog* messageLog;

    void IncrementMoveCount();
public:
    Player(int x, int y, MessageLog* messageLog);
    ~Player();
    PlayerTransport GetTransport() const;
    int GetX() const;
    int GetY() const;
    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();
    void Pass();
    void CycleTransportLeft();
    void CycleTransportRight();
    unsigned short GetHits() const;
    unsigned short GetFood() const;
    unsigned short GetExperience() const;
    unsigned short GetCoin() const;
};

#endif