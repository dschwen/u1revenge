#include <sstream>
#include <fstream>
#include <string>

#include "GameSettings.h"

string GameSettings::Trim(const string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

GameSettings::GameSettings()
{
}


GameSettings::~GameSettings()
{
}

void GameSettings::Load(const char* filePath)
{
    std::ifstream file(filePath, std::ios::in);
    std::string line;

    while (std::getline(file, line)) // read line by line
    {
        if (line.length() > 0)
        {
            size_t indexOfEquals = line.find('=');

            if (indexOfEquals != std::string::npos) // npos means '=' was not found
            {
                std::string key = line.substr(0, indexOfEquals);
                std::string value = line.substr(indexOfEquals + 1);

                if (key.length() > 0 && value.length() > 0)
                {
                    key = this->Trim(key);
                    value = this->Trim(value);

                    this->settings[key] = value;
                }
            }
        }
    }

    file.close();
}

string GameSettings::Get(string key, string defaultValue)
{
    auto iterator = this->settings.find(key);
    if (iterator == this->settings.end())
        return defaultValue;
    else
        return iterator->second;
}

int GameSettings::GetInt(string key, int defaultValue)
{
    string valueStr = this->Get(key);
    if (valueStr == "")
        return defaultValue;
    else
        return stoi(valueStr);
}

