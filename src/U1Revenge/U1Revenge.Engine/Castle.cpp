#include "Castle.h"

Castle::Castle(const char* name, int x, int y, int type)
    : Location(name, nullptr, x, y), type(type)
{
}

Castle::~Castle()
{
}

int Castle::GetType()
{
    return this->type;
}
