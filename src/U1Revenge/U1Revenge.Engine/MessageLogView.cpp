#include "MessageLogView.h"

MessageLogView::MessageLogView(MessageLog* messageLog, SDL_Renderer* renderer, TTF_Font* font)
    : View(renderer), messageLog(messageLog), fontColour({ 84, 255, 255 }),
        fontSurface(nullptr), fontTexture(nullptr), font(font)
{
}

MessageLogView::~MessageLogView()
{
    SDL_DestroyTexture(this->fontTexture);
    SDL_FreeSurface(this->fontSurface);
}

void MessageLogView::Render()
{
    auto renderer = this->GetRenderer();

    SDL_DestroyTexture(this->fontTexture);
    SDL_FreeSurface(this->fontSurface);

    std::string linesStr;
    this->messageLog->GetMessages(linesStr);

    // TODO recreating these every time seems very inefficient
    this->fontSurface = TTF_RenderText_Blended_Wrapped(font, linesStr.c_str(), this->fontColour, 500);
    this->fontTexture = SDL_CreateTextureFromSurface(renderer, fontSurface);

    SDL_Rect dstrect = { 8, 336, 0, 0 };
    SDL_QueryTexture(fontTexture, NULL, NULL, &dstrect.w, &dstrect.h);

    SDL_RenderCopy(renderer, this->fontTexture, NULL, &dstrect);
}
