#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include <SDL.h>
#include <SDL_ttf.h>

#include "Palette.h"
#include "Tileset.h"
#include "MessageLogView.h"
#include "ViewportView.h"
#include "BottomRightView.h"
#include "BorderView.h"
#include "GameSettings.h"

class GameEngine
{
private:
    int width;
    int height;
    Palette* palette;
    Tileset* tileset;
    bool fullScreen;
    SDL_Window* window;
    TTF_Font* font;
    SDL_Renderer* renderer;
    SDL_Surface* icon;
    BorderView* borderView;
    MessageLogView* messageLogView;
    BottomRightView* bottomRightView;
    ViewportView* viewportView;
    GameSettings* gameSettings;
public:
    GameEngine(int width, int height,
        MessageLog* messageLog, World* world, int viewportWidth, int viewportHeight,
        Player* player, GameSettings* gameSettings);
    ~GameEngine();
    TTF_Font* GetFont() const;
    SDL_Renderer* GetRenderer() const;
    void ToggleFullScreen();
    void Render() const;
};

#endif