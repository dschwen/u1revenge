#ifndef _BORDERVIEW_H_
#define _BORDERVIEW_H_

#include <SDL.h>

#include "View.h"

class BorderView : public View
{
private:
    SDL_Rect topBorder = { 0, 0, 640, 16 };
    SDL_Rect leftBorder = { 0, 0, 16, 320 };
    SDL_Rect rightBorder = { 624, 0, 16, 320 };
    SDL_Rect bottomBorder = { 0, 304, 640, 16 };
    SDL_Rect bottomSideBorder = { 483, 320, 12, 80 };
    SDL_Rect viewportTopHorizontalWhite = { 16, 14, 608, 2 };
    SDL_Rect viewportLeftVerticalWhite = { 14, 16, 2, 288 };
    SDL_Rect viewportRightVerticalWhite = { 624, 16, 2, 288 };
    SDL_Rect viewportBottomHorizontalWhite = { 16, 304, 608, 2 };
    SDL_Rect messageLogTopWhite = { 0, 318, 484, 2 };
    SDL_Rect messageLogRightWhite = { 483, 318, 2, 82 };
    SDL_Rect playerStatsLeftWhite = { 493, 318, 2, 82 };
    SDL_Rect playerStatsTopWhite = { 493, 318, 147, 2 };
public:
    BorderView(SDL_Renderer* renderer);
    ~BorderView();
    void Render();
};

#endif