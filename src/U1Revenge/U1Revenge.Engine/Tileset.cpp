#include <fstream>

#include "Palette.h"
#include "Tileset.h"

Tileset::Tileset(Palette* palette)
    : palette(palette), tileWidth(16), tileHeight(16), tileCount(52)
{
}

Tileset::~Tileset()
{
    delete[] this->pixels;
}

void Tileset::Load(const char* filePath)
{
    const int fileSize = 6656;
    std::ifstream file(filePath, std::ios::binary | std::ios::in);
    char * buffer = new char[fileSize];
    int pixelCount = fileSize * 2;
    this->pixels = new int[pixelCount];
    int pixelIndex = 0;

    if (file.read(buffer, fileSize))
    {
        // convert file bytes to pixel colour array

        for (int i = 0; i < fileSize; i++) // goes through each byte
        {
            char currentByte = buffer[i];
            char paletteIndex = 0; // this is the pixel value, to be translated to colour later

            for (int j = 0; j < 2; j++) // determines whether to take high or low half-byte
            {
                if (j == 0)
                    paletteIndex = (currentByte & 0xF0) >> 4;
                else
                    paletteIndex = currentByte & 0x0F;

                int pixelColour = this->palette->IndexToColour(paletteIndex);
                pixels[pixelIndex] = pixelColour;
                pixelIndex++;
            }
        }
    }
    else
    {
        // TODO handle error case
    }

    file.close();
}

int * Tileset::GetPixels(char tileIndex) const
{
    // here we get a reference to the first pixel in the requested tile

    return this->pixels + (this->tileWidth * this->tileHeight * tileIndex);
}
