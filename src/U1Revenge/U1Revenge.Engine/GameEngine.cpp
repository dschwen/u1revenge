#include "GameEngine.h"

GameEngine::GameEngine(int width, int height,
    MessageLog* messageLog, World* world, int viewportWidth, int viewportHeight,
    Player* player, GameSettings* gameSettings)
    : width(width), height(height), fullScreen(false),
        gameSettings(gameSettings)
{
    string u1Path = gameSettings->Get("U1Path", "C:/games/Ultima 1");
    string graphics = gameSettings->Get("Graphics", "CGA");
    int scale = gameSettings->GetInt("Scale", 2);

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    this->window = SDL_CreateWindow("U1Revenge Engine 3",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width * scale, height * scale, 0);
    this->font = TTF_OpenFont("pcsenior.ttf", 16);

    this->renderer = SDL_CreateRenderer(this->window, -1, 0);

    std::string tilesetFilePath = u1Path + '/' + graphics + "TILES.BIN"; // e.g. T1KTILES.BIN
    this->palette = new Palette();
    this->tileset = new Tileset(this->palette);
    this->tileset->Load(tilesetFilePath.c_str());

    // set window icon

    int* iconPixels = this->tileset->GetPixels(48);
    int iconPixelsWithTransparency[256];
    memcpy(iconPixelsWithTransparency, iconPixels, 256 * sizeof(int));
    
    for (int i = 0; i < 256; i++) // set all black pixels to transparent
    {
        if (iconPixelsWithTransparency[i] == 0xFF000000)
            iconPixelsWithTransparency[i] = 0x00000000; // turn off opacity, i.e. alpha = 0
    }

    this->icon = SDL_CreateRGBSurfaceWithFormatFrom(iconPixelsWithTransparency, 16, 16, 32,
        16 * sizeof(int), SDL_PIXELFORMAT_ARGB8888);
    SDL_SetWindowIcon(window, icon);

    // create views

    this->borderView = new BorderView(renderer);
    this->messageLogView = new MessageLogView(messageLog, renderer, font);
    this->bottomRightView = new BottomRightView(renderer, font, player, scale);
    this->viewportView = new ViewportView(renderer, width, height, viewportWidth, viewportHeight,
        world, tileset, player);
}

GameEngine::~GameEngine()
{
    delete this->viewportView;
    delete this->bottomRightView;
    delete this->messageLogView;
    delete this->borderView;
    delete this->tileset;
    delete this->palette;

    SDL_FreeSurface(this->icon);

    SDL_DestroyRenderer(this->renderer);

    TTF_CloseFont(this->font);
    SDL_DestroyWindow(this->window);

    TTF_Quit();
    SDL_Quit();
}

TTF_Font * GameEngine::GetFont() const
{
    return this->font;
}

SDL_Renderer * GameEngine::GetRenderer() const
{
    return this->renderer;
}

void GameEngine::ToggleFullScreen()
{
    this->fullScreen = !this->fullScreen;
    SDL_SetWindowFullscreen(this->window, this->fullScreen ? SDL_WINDOW_FULLSCREEN : 0);
}

void GameEngine::Render() const
{
    // clear all to black

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // black
    SDL_RenderClear(renderer);

    // render viewport, message log and bottom right

    this->viewportView->Render();
    this->messageLogView->Render();
    this->bottomRightView->Render();

    // render borders - note these don't scale at the moment

    this->borderView->Render();

    // swap video buffers

    SDL_RenderPresent(renderer);
}
