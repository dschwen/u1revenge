#include "View.h"

SDL_Renderer* View::GetRenderer()
{
    return this->renderer;
}

View::View(SDL_Renderer* renderer)
    : renderer(renderer)
{
}

View::~View()
{
}
