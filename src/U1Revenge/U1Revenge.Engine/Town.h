#ifndef _TOWN_H_
#define _TOWN_H_

#include "Location.h"

class Town : public Location
{
private:
    int type;
public:
    Town(const char* name, int type, int x, int y);
    ~Town();
    int GetType();
};

#endif