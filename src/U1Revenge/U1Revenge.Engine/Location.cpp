#include "Location.h"

using std::string;

Location::Location(const char* name, const char* prefix, int x, int y)
    : name(name), x(x), y(y)
{
    this->fullName = new string;
    if (prefix != nullptr)
        this->fullName->append(prefix);
    this->fullName->append(name);
}

Location::~Location()
{
    delete this->fullName;
}

const char * Location::GetName()
{
    return this->name;
}

const char* Location::GetFullName()
{
    return this->fullName->c_str();
}

int Location::GetX() const
{
    return this->x;
}

int Location::GetY() const
{
    return this->y;
}

int Location::GetLocationKey(int x, int y)
{
    int key = y * 168 + x;
    return key;
}